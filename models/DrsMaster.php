<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%drs_master}}".
 *
 * @property int $id
 * @property string $drs_id
 * @property string $executive_name
 * @property string $contact_no
 * @property string $vehicle_no
 * @property string $location
 * @property int $no_of_jobs
 * @property string $date
 * @property string $created_date
 * @property int $created_by
 * @property string $update_date
 * @property int $updated_by
 * @property int $active
 */
class DrsMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%drs_master}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['drs_id', 'executive_name', 'contact_no', 'vehicle_no', 'location', 'no_of_jobs', 'date', 'created_date', 'created_by', 'update_date', 'updated_by'], 'required'],
            [['no_of_jobs', 'created_by', 'updated_by', 'active'], 'integer'],
            [['created_date', 'update_date'], 'safe'],
            [['drs_id'], 'string', 'max' => 25],
            [['contact_no'],'integer'],
            [['executive_name', 'contact_no'], 'string', 'max' => 255],
            [['vehicle_no', 'location', 'date'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'drs_id' => Yii::t('app', 'Drs ID'),
            'executive_name' => Yii::t('app', 'Executive Name'),
            'contact_no' => Yii::t('app', 'Contact No'),
            'vehicle_no' => Yii::t('app', 'Vehicle No'),
            'location' => Yii::t('app', 'Location'),
            'no_of_jobs' => Yii::t('app', 'No Of Jobs'),
            'date' => Yii::t('app', 'Date'),
            'created_date' => Yii::t('app', 'Created Date'),
            'created_by' => Yii::t('app', 'Created By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'active' => Yii::t('app', 'Active'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return DrsMasterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DrsMasterQuery(get_called_class());
    }
}
