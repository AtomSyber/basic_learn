<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DrsMaster */

$this->title = Yii::t('app', 'Create Drs Master');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Drs Masters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="drs-master-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
